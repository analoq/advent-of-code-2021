// split by newline
// split lines by spaces
pub fn parse(input: &'static str) -> Vec<Vec<&'static str>>{
		let x: Vec<Vec<&str>> = input	.split("\n")
				.collect::<Vec<&str>>()
				.iter()
				.map(|s| s.split(" ").collect())
				.collect();
		x[0..x.len()-1].to_vec()
}

