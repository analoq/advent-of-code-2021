use crate::utils;

fn translate(input: Vec<Vec<&'static str>>) -> Vec<(&str, i32)> {
	input	.iter()
			.map(|x| (x[0], x[1].parse::<i32>().unwrap()))
			.collect()
}

pub fn task1(){
	let input = include_str!("../../input/day02.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day02.txt");
	println!("  Task 2: {}", task2_sub(input));
	
}

fn task1_sub(input: &'static str) -> i32{
	let input = utils::parse(input);
	let input = translate(input);
	let mut forward = 0;
	let mut depth = 0;
	for x in input.iter(){
		match x.0{
			"forward" => forward += x.1,
			"down" => depth += x.1,
			"up" => depth -= x.1,
			_ => (),
		}
	}
	forward * depth
}

fn task2_sub(input: &'static str) -> i32{
	let input = utils::parse(input);
	let input = translate(input);
	let mut aim = 0;
	let mut depth = 0;
	let mut forward = 0;
	for x in input.iter(){
		match x.0{
			"forward" => {
				forward += x.1;
				depth += x.1*aim;
			},
			"down" => aim += x.1,
			"up" => aim -= x.1,
			_ => (),
		}
	}
	forward * depth
}

#[cfg(test)]
mod tests {
	use crate::day02::task1_sub;
	use crate::day02::task2_sub;

    #[test]
    fn task1() {
		let input = "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2\n";
        assert_eq!(task1_sub(input), 150);
    }

    #[test]
    fn task2() {
		let input = "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2\n";
        assert_eq!(task2_sub(input), 900);
    }
}
