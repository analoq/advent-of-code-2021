pub fn task1(){
	let input = include_str!("../../input/day04.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day04.txt");
	println!("  Task 2: {}", task2_sub(input));
}

fn task1_sub(input: &'static str) -> i32{
	let stack = make_stack(&input);
	let mut boards = make_boards(&input);

	for number in stack{
		for i in 0..boards.len(){
			boards[i] = flag_number_on_board(boards[i].clone(), number);
			if check_for_win(&boards[i]) {
				return calculate_answer(&boards[i], number);
			}
		}
	}
	-1
}

fn task2_sub(input: &'static str) -> i32{
	let stack = make_stack(&input);
	let mut boards = make_boards(&input);
	
	let mut finalscore = 0;
	let mut finishedboards: Vec<usize> = vec![];
	for number in stack{
		for i in 0..boards.len(){
			boards[i] = flag_number_on_board(boards[i].clone(), number);
			let a = finishedboards	.iter()
									.filter(|x| **x == i)
									.collect::<Vec<&usize>>()
									.len();
			if a == 0{
				if check_for_win(&boards[i]) {
					finishedboards.push(i);
					finalscore = calculate_answer(&boards[i], number);
				}
			}
		}
	}
	finalscore
}

fn make_stack(input: &'static str) -> Vec<i32> {
	let input: Vec<&str>= input.split("\n\n").collect();
	input[0].split(",")
			.collect::<Vec<&str>>()
			.iter()
			.map(|x| x.parse::<i32>().unwrap())
			.collect::<Vec<i32>>()
}

fn make_boards(input: &'static str) -> Vec<Vec<Vec<i32>>> {
	let input: Vec<&str>= input.split("\n\n").collect();

	let mut boards: Vec<Vec<Vec<i32>>> = vec![];
	for board in input[1..input.len()].iter(){
		let board = board	.split("\n")
							.collect::<Vec<&str>>();
		let mut b: Vec<Vec<i32>> = vec![];
		for row in board{
			let x = row	.split(" ")
						.collect::<Vec<&str>>()
						.into_iter()
						.filter(|x| x.len() > 0)
						.collect::<Vec<&str>>()
						.iter()
						.map(|x| x.parse::<i32>().unwrap())
						.collect::<Vec<i32>>();
			b.push(x);
		}
		boards.push(b);
	}
	boards
}

fn flag_number_on_board(board: Vec<Vec<i32>>, num: i32) -> Vec<Vec<i32>>{
	let mut b: Vec<Vec<i32>> = vec![];
	for row in board{
		b.push(row.into_iter().map(|x| if x==num {-1} else {x}).collect::<Vec<i32>>());
		
	}
	b
}

fn check_for_win(board: &Vec<Vec<i32>>) -> bool {
	for row in board{
		if row.iter().fold(0, |acc, x| acc+x) == -5{
			return true;
		}
	}
	for x in 0..5{
		let mut sum = 0;
		for y in 0..5{
			sum += board[y][x]
		}
		if sum == -5{
			return true;
		}
	}
	false
}

fn calculate_answer(board: &Vec<Vec<i32>>, num: i32) -> i32 {
	board	.iter()
			.flatten()
			.filter(|x| **x>-1)
			.sum::<i32>() * num
}

#[cfg(test)]
mod tests{
	use crate::day04::task1_sub;
	use crate::day04::task2_sub;


	#[test]
	fn task1(){
		let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,\
					6,15,25,12,22,18,20,8,19,3,26,1\n\
		\n\
		22 13 17 11  0\n\
		 8  2 23  4 24\n\
		21  9 14 16  7\n\
		 6 10  3 18  5\n\
		 1 12 20 15 19\n\
		\n\
		 3 15  0  2 22\n\
		 9 18 13 17  5\n\
		19  8  7 25 23\n\
		20 11 10 24  4\n\
		14 21 16 12  6\n\
		\n\
		14 21 17 24  4\n\
		10 16 15  9 19\n\
		18  8 23 26 20\n\
		22 11 13  6  5\n\
		 2  0 12  3  7\n";
		assert_eq!(task1_sub(input), 4512);
	}
	#[test]
	fn task2(){
		let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,\
					6,15,25,12,22,18,20,8,19,3,26,1\n\
		\n\
		22 13 17 11  0\n\
		 8  2 23  4 24\n\
		21  9 14 16  7\n\
		 6 10  3 18  5\n\
		 1 12 20 15 19\n\
		\n\
		 3 15  0  2 22\n\
		 9 18 13 17  5\n\
		19  8  7 25 23\n\
		20 11 10 24  4\n\
		14 21 16 12  6\n\
		\n\
		14 21 17 24  4\n\
		10 16 15  9 19\n\
		18  8 23 26 20\n\
		22 11 13  6  5\n\
		 2  0 12  3  7\n";
		assert_eq!(task2_sub(input), 1924);
	}
}
