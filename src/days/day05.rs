use std::collections::{HashMap};

pub fn task1(){
	let input = include_str!("../../input/day05.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day05.txt");
	println!("  Task 2: {}", task2_sub(input));
}


fn task1_sub(input: &'static str) -> usize {

	let input = convert_input(input);

	let input: Vec<Vec<Vec<i32>>> = 
			input	.into_iter()
					.filter(|x| x[0][0] == x[1][0] || x[0][1] == x[1][1]).collect();

	let mut thermal_map = HashMap::new();

	for thermal in &input{
		for s in vent_line(thermal) {
			let index = [s.0.to_string(), s.1.to_string()].join("-");
			let val = match thermal_map.get(&*index){
				Some(&number) => number,
				_ => 0
			};
			thermal_map.insert(index, val+1);
		}
	}

	let mut result = 0;
	for (_,v) in thermal_map{
		if v > 1{
			result += 1;
		}
	}

	result
}

fn task2_sub(input: &'static str) -> usize {
	let input = convert_input(input);

	let mut thermal_map = HashMap::new();
	for thermal in &input{
		for s in vent_line(thermal) {
			let index = [s.0.to_string(), s.1.to_string()].join("-");
			let val = match thermal_map.get(&*index){
				Some(&number) => number,
				_ => 0
			};
			thermal_map.insert(index, val+1);
		}
	}

	let mut result = 0;
	for (_,v) in thermal_map{
		if v > 1{
			result += 1;
		}
	}

	result
}

fn vent_line(vent: &Vec<Vec<i32>>) -> Vec<(i32,i32)> {
    let x1 = vent[0][0];
    let y1 = vent[0][1];
    let x2 = vent[1][0];
    let y2 = vent[1][1];

    let dx = x2-x1;
    let dy = y2-y1;
    
    let iter_x;
    if dx>0 {
        iter_x = (0..=dx).collect::<Vec<i32>>();
    } else {
        iter_x = (dx..=0).rev().collect::<Vec<i32>>();
    }

    let iter_y;
    if dy>0 {
        iter_y = (0..=dy).collect::<Vec<i32>>();
    } else {
        iter_y = (dy..=0).rev().collect::<Vec<i32>>();
    }
    
    let result: Vec<(i32,i32)>;
    if iter_x.len() == iter_y.len(){
		result = iter_x.into_iter().zip(iter_y.into_iter()).collect();
    } else if iter_x.len() > iter_y.len(){
        result = iter_x.into_iter().map(|x| (x,0)).collect();
    } else {
        result = iter_y.into_iter().map(|x| (0,x)).collect();
    }
	result.iter().map(|x| (x.0+x1, x.1+y1)).collect()
}

fn convert_input(input: &'static str) -> Vec<Vec<Vec<i32>>> {
	input	.split("\n")
			.filter(|x| x.len() > 0)
			.map(|x| x.split(" -> ")
				.map(|x| x.split(",").map(|x| x.parse::<i32>().unwrap())
					.collect::<Vec<i32>>())
				.collect::<Vec<Vec<i32>>>())
			.collect::<Vec<Vec<Vec<i32>>>>()
}

#[cfg(test)]
mod tests{
	use crate::days::day05::task1_sub;
	use crate::days::day05::task2_sub;
	#[test]
	fn task1(){
		let input = "0,9 -> 5,9\n\
					8,0 -> 0,8\n\
					9,4 -> 3,4\n\
					2,2 -> 2,1\n\
					7,0 -> 7,4\n\
					6,4 -> 2,0\n\
					0,9 -> 2,9\n\
					3,4 -> 1,4\n\
					0,0 -> 8,8\n\
					5,5 -> 8,2\n";
		assert_eq!(task1_sub(input), 5);
	}

	#[test]
	fn task2(){
		let input = "0,9 -> 5,9\n\
					8,0 -> 0,8\n\
					9,4 -> 3,4\n\
					2,2 -> 2,1\n\
					7,0 -> 7,4\n\
					6,4 -> 2,0\n\
					0,9 -> 2,9\n\
					3,4 -> 1,4\n\
					0,0 -> 8,8\n\
					5,5 -> 8,2\n";
		assert_eq!(task2_sub(input), 12);
	}
}
