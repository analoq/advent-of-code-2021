use crate::utils;

fn translate(input: Vec<Vec<&'static str>>) -> Vec<i32> {
	input	.iter()
			.map(|x| x[0].parse::<i32>().unwrap())
			.collect()
}

pub fn task1(){
	let input = include_str!("../../input/day01.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day01.txt");
	println!("  Task 2: {}", task2_sub(input));
}

fn task1_sub(input: &'static str) -> usize{
	let input = utils::parse(input);
	let input = translate(input);
	input.windows(2).filter(|x| x[0]<x[1]).count()
}

fn task2_sub(input: &'static str) -> usize{
	let input = utils::parse(input);
	let input = translate(input);
	input	.windows(3)
			.map(|x| x.iter().sum())
			.collect::<Vec<i32>>()
			.windows(2)
			.filter(|x| x[0] < x[1])
			.count()
}

#[cfg(test)]
mod tests{
	use crate::day01::task1_sub;
	use crate::day01::task2_sub;

	#[test]
	fn task1(){
		let input = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263\n";
		assert_eq!(task1_sub(input),7);
	}

	#[test]
	fn task2(){
		let input = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263\n";
		assert_eq!(task2_sub(input),5);
	}
}
