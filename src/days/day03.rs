use crate::utils;

fn translate(input: Vec<Vec<&'static str>>) -> Vec<Vec<char>> {
	input	.iter()
			.map(|x| x[0].chars().collect())
			.collect()
}

pub fn task1(){
	let input = include_str!("../../input/day03.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day03.txt");
	println!("  Task 2: {}", task2_sub(input));
}

fn task1_sub(input: &'static str) -> i32{
	let input = utils::parse(input);
	let input = translate(input);
	let rows = input.len();
	let cols = input[0].len();
	let mut gamma: i32 = 0;
	let mut sigma: i32 = 0;
	for i in 0..cols{
		let r = input.iter().filter(|x| x[i] > '0').count();
		if r < rows/2 {
			gamma &= !(1 << (cols-i-1));
			sigma |= 1 << (cols-i-1);
		} else {
			gamma |= 1 << (cols-i-1);
			sigma &= !(1 << (cols-i-1));
		}
	}
	gamma * sigma
}

fn task2_sub(input: &'static str) -> i32{
	let input = utils::parse(input);
	let input = translate(input);

	let generator = reduce(input.clone(), 0, '1');
	let scrubber = reduce(input, 0, '0');

	generator * scrubber	
}

fn reduce(data: Vec<Vec<char>>, i: usize, c: char) -> i32 {
	if data.len() > 1{
		let d = dominant_char(&data, i);
		let dat = data.clone();
		match c {
			'1' =>{
				let dat = dat	.into_iter()
								.filter(|x| x[i] == d)
								.collect();
				reduce(dat, i+1, c)
			}
			_   =>{
				let dat = dat	.into_iter()
								.filter(|x| x[i] != d)
								.collect();
				reduce(dat, i+1, c)
			}
		}
	} else {
		return vec_to_int(data[0].clone())
	}
}

fn vec_to_int(data: Vec<char>) -> i32{
	let width = data.len();
	let mut g: i32 = 0;
	for i in 0..data.len(){
		match data[i]{
			'0' => g &= !(1<< (width - i - 1)),
			_   => g |= 1<<(width - i - 1),
		}
	}	
	g
}

fn dominant_char(data: &Vec<Vec<char>>, i: usize) -> char{
	let one = data.iter().filter(|x| x[i] == '1').count();
	let zero= data.iter().filter(|x| x[i] == '0').count();
	if one > zero {
		return '1';
	} else if one < zero {
		return '0';
	} else {
		return '1';
	}
}


#[cfg(test)]
mod tests{
	use crate::day03::task1_sub;
	use crate::day03::task2_sub;

	#[test]
	fn task1(){
		let input = "00100\n11110\n10110\n10111\n10101\n01111\n00111\n\
					11100\n10000\n11001\n00010\n01010\n";
		assert_eq!(task1_sub(input), 198);
	}

	#[test]
	fn task2(){
		let input = "00100\n11110\n10110\n10111\n10101\n01111\n00111\n\
					11100\n10000\n11001\n00010\n01010\n";
		assert_eq!(task2_sub(input), 230);
	}

}

