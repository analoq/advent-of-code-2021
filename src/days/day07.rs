pub fn task1(){
	let input = include_str!("../../input/day07.txt");
	println!("  Task 1: {}", task1_sub(input));
}

pub fn task2(){
	let input = include_str!("../../input/day07.txt");
	println!("  Task 2: {}", task2_sub(input));
}

fn task1_sub(input: &'static str) -> i32 {
	let input = input.split('\n').collect::<Vec<&str>>()[0];
	let mut input = input   .split(',')
							.map(|x|x.parse::<i32>().unwrap())
							.collect::<Vec<i32>>();
	input.sort_unstable();
	let median = input[input.len()/2];
	calculate_fuel(&input, median)
}

fn calculate_fuel(input: &[i32], depth: i32) -> i32 {
	input   .iter()
			.map(|x| (x - depth).abs())
			.sum()
}

fn task2_sub(input: &'static str) -> i32 {
	let input = input.split('\n').collect::<Vec<&str>>()[0];
	let mut input = input   .split(',')
							.map(|x|x.parse::<i32>().unwrap())
							.collect::<Vec<i32>>();

	let mut optimum = 0;
	for i in 0..*input.iter().max().unwrap(){
		let tmp = calculate_fuel2(&input, i);
		if tmp < optimum || i == 0{
			optimum = tmp;
		}
	}
	optimum
}

fn calculate_fuel2(input: &[i32], depth: i32) -> i32 {
	input	.iter()
			.map(|x| ((x-depth).abs()*((x-depth).abs()+1))/2)
			.sum()
}

#[cfg(test)]
mod tests{
	use crate::days::day07::task1_sub;
	use crate::days::day07::task2_sub;

	#[test]
	fn task1(){
		let input = "16,1,2,0,4,2,7,1,2,14\n";
		assert_eq!(task1_sub(input), 37); 
	}
	#[test]
	fn task2(){
		let input = "16,1,2,0,4,2,7,1,2,14\n";
		assert_eq!(task2_sub(input), 168); 
	}
}

