
pub fn task1(){
	let input = include_str!("../../input/day06.txt");
	println!("  Task 1: {}", task_sub(input, 80));
}

pub fn task2(){
	let input = include_str!("../../input/day06.txt");
	println!("  Task 2: {}", task_sub(input, 256));
}

fn task_sub(input: &'static str, gen: u32) -> u64 {
	let input: Vec<u32> = input	.split(',')
								.filter(|x| x.len()>0 && *x != "\n")
								.map(|x| x.chars().next().unwrap().to_digit(10).unwrap())
								.collect();

	let mut bucket: Vec<u64> = vec![0,0,0,0,0,0,0,0,0];

	for i in input.iter(){
		bucket[*i as usize] += 1;
	}
	let result: u64 = asdf(bucket, gen-1).into_iter().sum();

	result
}

fn asdf(bucket: Vec<u64>, gen: u32) -> Vec<u64> {
	let mut tmp: Vec<u64> = vec![0,0,0,0,0,0,0,0,0];
	for i in 0..=7 {
		tmp[i] = bucket[i+1];
	}
	tmp[6] += bucket[0];
	tmp[8] = bucket[0];
	if gen > 0{
		asdf(tmp, gen-1)
	} else {
		tmp
	}
}

#[cfg(test)]
mod tests{
	use crate::days::day06::task_sub;

	#[test]
	fn task1(){
		let input = "3,4,3,1,2";
		assert_eq!(task_sub(input, 80), 5934);
	}
	#[test]
	fn task2(){
		let input = "3,4,3,1,2";
		assert_eq!(task_sub(input, 256), 26984457539);
	}
}
