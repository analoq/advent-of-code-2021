mod utils;
mod days;
use days::*;

fn main(){
/*
	println!("Day  1"); day01::task1(); day01::task2();
	println!("Day  2"); day02::task1(); day02::task2();
	println!("Day  3"); day03::task1(); day03::task2();
	println!("Day  4"); day04::task1(); day04::task2();
	println!("Day  5"); day05::task1(); day05::task2();
	println!("Day  6"); day06::task1(); day06::task2();
*/
	println!("Day  7"); day07::task1(); day07::task2();
}
